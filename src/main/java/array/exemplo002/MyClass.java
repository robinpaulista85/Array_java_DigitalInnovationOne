package array.exemplo002;

public class MyClass {
    public static void main(String[] args) {
        int[] meuArray = {12,32,54,6,8,89,64}; //inicia em 0 e vai até 7.

        //percorrendo arrays

        for(int i=0; i<7; i++){
            System.out.println(meuArray[i]);

        // for(int i=0; i< meuArray.length; i++){ Esse exemplo é igual o de cima, só muda o nome da iteração
            System.out.println(meuArray[i]);//
        }
    }
}
